#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "opencv2/features2d/features2d.hpp"
#include "ofxCv.h"
#include "UIGrid.h"

using namespace cv;
using namespace ofxCv;

class testApp : public ofBaseApp{
	private:
		//ofImage					img;
		//ofxCvColorImage		colorImage;
		ofxCvGrayscaleImage		grayImage;
		//SurfFeatureDetector		surfDetector;

		vector<KeyPoint>		keyPoints;
		vector<ofPoint>			corners;

		UIElement element_sideArt, e_featureTxt, e_ratingTxt;
		UIGrid *grid;
		vector<UIElement> dots;

		void featuresFinder(ofImage &img);
		void processOpenedFile(ofFileDialogResult result);
		void updateStat();

	public:
		vector<string> args;
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
