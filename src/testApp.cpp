#include "testApp.h"

float scale = 512.0f;
int sw = 512;
int sh = 264;
string stats;
int winw, winh, gridw, gridh;

ofImage img;
Mat m;
Mat outNorm, outNormScaled;

//--------------------------------------------------------------
void testApp::setup(){
	ofSetFrameRate(30);
	ofBackground(22, 23, 26);	
	ofEnableAlphaBlending();

	img.loadImage("1.jpg");
	img.setImageType(OF_IMAGE_GRAYSCALE);

	bool isPortrait = img.width >= img.height ? false : true;
	float r;
	int newW, newH;

	if(isPortrait){
		r = (float) img.height / (float) img.width;
		newW = (int) (scale / r);
		newH = scale;
	}else{
		r = (float) img.width / (float)img.height;
		newW = scale;
		newH = (int) (scale / r);		
	}
	
	img.resize(newW, newH);
	sw = img.width;
	sh = img.height;

	featuresFinder(img);

	gridw = sw + 48;
	gridh = sh + 48;
	grid = new UIGrid(gridw, gridh);

	element_sideArt = UIElement(UI_TYPE_SIDEART);
	element_sideArt.loadImage();

	e_featureTxt = UIElement(UI_TYPE_LABEL_VALUE_PAIR);	
	e_ratingTxt = UIElement(UI_TYPE_LABEL_VALUE_PAIR);

	e_featureTxt.setLabel("Rating");
	e_featureTxt.setValue("5");

	winw = ofGetWidth();
	winh = ofGetHeight();
}

//--------------------------------------------------------------
void testApp::update(){
	ofSetWindowTitle(ofToString(ofGetFrameRate()));
}

//--------------------------------------------------------------
void testApp::draw(){
	ofSetColor(180);
	img.draw(winw * 0.5 - sw * 0.5, winh * 0.5 - sh * 0.5, sw, sh);
	//grayImage.draw(0,0);
	
	ofPushMatrix();
	ofTranslate(winw * 0.5 - sw * 0.5, winh * 0.5 - sh * 0.5);
	if(keyPoints.size() > 0){
		for(int i = keyPoints.size()-1; i >= 0; i--){
			//ofSetColor(255,0,0);
			//ofCircle(keyPoints[i].pt.x, keyPoints[i].pt.y, 2);
			dots[i].draw(keyPoints[i].pt.x, keyPoints[i].pt.y);
		}
	}
	ofPopMatrix();

	ofSetColor(240);
	ofDrawBitmapString(stats, ofGetWidth() - 120, 20, 0);

	grid->draw (winw * 0.5 - gridw * 0.5, winh * 0.5 - gridh * 0.5 + 6);
	element_sideArt.draw(0,0);

	e_featureTxt.draw(100, 100);
	//e_ratingTxt->draw(100, 125);
}

void testApp::processOpenedFile(ofFileDialogResult result){
	ofFile file(result.getPath());

	if(file.exists()){
		string fileExtension = ofToUpper(file.getExtension());

		if (fileExtension == "JPG" || fileExtension == "PNG") {
			img.loadImage(result.getPath());
			img.setImageType(OF_IMAGE_GRAYSCALE);

			bool isPortrait = img.width >= img.height ? false : true;
			float r;
			int newW, newH;

			if(isPortrait){
				r = (float) img.height / (float) img.width;
				newW = (int) (scale / r);
				newH = scale;
			}else{
				r = (float) img.width / (float)img.height;
				newW = scale;
				newH = (int) (scale / r);		
			}
	
			img.resize(newW, newH);
			sw = img.width;
			sh = img.height;

			featuresFinder(img);
		}
	}
}

void testApp::featuresFinder(ofImage &img) {
	m = toCv(img);
	Mat outHC = Mat::zeros(m.size(), CV_32FC1);

	//SurfFeatureDetector surfDetector(400);
	GoodFeaturesToTrackDetector surfDetector(1000, 0.1, 5, 3, false, 0.04);
	surfDetector.detect(m, keyPoints);

	dots.resize(keyPoints.size());
	for (int i = 0; i < keyPoints.size(); i++) {
		UIElement e(UI_TYPE_POINT);
		dots[i] = e;
	}
	
	//cout << "Features: " << keyPoints.size() << endl;
	//cout << "Rating: " << ofMap(keyPoints.size(), 0, 100, 0, 5, true) << endl;

	updateStat();	
}

void testApp::updateStat() {
	string fsize = ofToString(keyPoints.size());
	string rating = ofToString(ofMap(keyPoints.size(), 0, 100, 0, 5, true));

	stats = "Features: " + fsize;
	stats += "\nRating: " + rating;

	//e_featureTxt.setLabel("Rating");
	e_featureTxt.setValue(rating);

	//e_ratingTxt->_label = "Rating";
	//e_ratingTxt->_value = rating;

	cout << stats <<endl;
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	if(key == ' '){
		ofFileDialogResult openFileResult = ofSystemLoadDialog("Select a jpg or png");

		if(openFileResult.bSuccess) {
			processOpenedFile(openFileResult);
		} else {
			//canceled
		}
	}
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	winw = w;
	winh = h;
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}
